$Id

Overview
--------
This module allows Drupal to replace textarea input fields with the standards-compliant XStandard WYSIWYG editor.


Compatibility
-------------
The XStandard plugin currently supports the following platforms and browsers:
 - on Windows 2000, XP and Vista in IE 5.0+, Firefox 1.0+, Safari 3.0+ and Opera 9.0+
 - on OS X 10.3.9+ in Firefox 1.0+ and Safari 1.3+


Required components
-------------------
To use XStandard in Drupal you need to download and install XStandard on the client computer(s).
http://xstandard.com/en/downloads/


More information and licence
----------------------------
XStandard is a proprietary product from Belus Technology, Inc.
For more information, go to: http://www.xstandard.com/


Installing the editor
---------------------
1. Download and install the editor plugin on your client computer
http://xstandard.com/en/downloads/

2. (Optional) Upload any custom configuration files you wish to use to the empty "files" subfolder in the xstandard module folder:

format.css
preview.xsl
screenreader.xsl
localization.xml
icons.xml
buttons.xml
styles.xml

Files from localization packs also go in this folder. Make sure they have a corresponding file name suffix, e.g. "localization-sv.xml" and "buttons-sv.xml"for Swedish.

3. In Drupal, go to "Administer" -> "Site building" -> "Modules" and enable the XStandard module.

4. Go to the settings page for the XStandard module ("Administer" -> "Site configuration" -> "XStandard") and configure the module. Note that if the field "Fields that should be edited with XStandard" is empty the module will never be used. You can get the id values for the fields you want to enable XStandard for by going to the form in question logged in as user 1. Under each textarea input field there is a line of text that reads something like "Use XStandard to edit Something content by including this id: edit-something". Insert that id in the "Fields that should be edited with XStandard" field.


Adding custom buttons for <!--break--> and <!--pagebreak--> (Pro version)
-------------------------------------------------------------------------
1. Enable custom buttons on the XStandard module Pro settings page.
2. Add the following inside the <buttons> tag in your buttons.xml file (adjust as needed for a localized version):

<snippet>
	<id>break</id>
	<name xml:lang="en">Teaser break</name>
	<name xml:lang="xx">TODO</name>
	<value>&lt;!--break--&gt;</value>
	<icon>pagebreak</icon>
</snippet>
<snippet>
	<id>pagebreak</id>
	<name xml:lang="en">Page break</name>
	<name xml:lang="xx">TODO</name>
	<value>&lt;!--pagebreak--&gt;</value>
	<icon>pagebreak</icon>
</snippet>

3. Create a custom toolbar set with the two new buttons by adding it to ToolbarWysiwyg on the XStandard Standard settings page, for example:
ordered-list, unordered-list, definition-list,, draw-layout-table, draw-data-table, image, separator, hyperlink, attachment, directory, spellchecker,, wysiwyg, source, preview, screen-reader, help, break, pagebreak


Configuring Web Services for Images and Attachments (Requires "Pro" license)
----------------------------------------------------------------------------
Download the PHP web services package from xstandard.com
Unzip the archive and put the files you need in the empty "services" subfolder, e.g.:

spellchecker.php
spellchecker.config
attachmentlibrary.config
attachmentlibrary.php
directory.config
directory.php
imagelibrary.config
imagelibrary.php
linklibrary.php

If you want to save uploaded files to the Drupal "files" folder, edit attachmentlibrary.php and imagelibrary.php and use the following paths:

define("XS_BASE_URL", '/files/');
define("XS_LIBRARY_FOLDER", "/var/www/mysite/files"); 

Also, make sure to enter your XS_AUTHORIZATION_CODE in the .php files to prevent unauthorized access.

For more information on how to configure the web services, go to:
http://xstandard.com/en/documentation/xstandard-dev-guide/web-services/library/

